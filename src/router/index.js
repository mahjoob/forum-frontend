import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Register from "@/views/Auth/Register";
import Login from "@/views/Auth/Login";
import SingleThread from "@/views/Thread/SingleThread";
import CreateThread from "@/views/Thread/CreateThread";

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/register',
    name: 'Register',
    component: Register
  },
  {
    path: '/login',
    name: 'Login',
    component: Login
  },
  {
    path: '/thread/:slug',
    name: 'single thread',
    component: SingleThread
  },
  {
    path: '/create/thread',
    name: 'Create Thread',
    component: CreateThread
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  }
]

const router = new VueRouter({
  routes
})

export default router
