import Axios from "../axios";

export let threadListRequest = (page) => {
    return Axios.get(`threads?page=${page}`)
};